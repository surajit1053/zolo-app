package com.zolo.customer.utils;

import android.content.Context;
import android.text.method.PasswordTransformationMethod;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.zolo.customer.R;

/**
 * Created by surajit on 9/9/17.
 */

public class CommonMethods {
    public static CommonMethods instance;

    private CommonMethods() {

    }

    public static CommonMethods getInstance() {
        if (instance == null) {
            instance = new CommonMethods();
        }
        return instance;
    }

    public void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }


    public boolean setPasswordIconStatus(EditText et, ImageView img, boolean isChecked) {
        int start, end;
        if (!isChecked) {
            img.setImageResource(R.drawable.design_ic_visibility_off);
            start = et.getSelectionStart();
            end = et.getSelectionEnd();
            et.setTransformationMethod(new PasswordTransformationMethod());
            et.setSelection(start, end);
            isChecked = true;
        } else {
            img.setImageResource(R.drawable.design_ic_visibility);
            start = et.getSelectionStart();
            end = et.getSelectionEnd();
            et.setTransformationMethod(null);
            et.setSelection(start, end);
            isChecked = false;
        }

        return isChecked;
    }
}
