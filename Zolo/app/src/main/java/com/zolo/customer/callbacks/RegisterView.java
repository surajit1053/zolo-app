package com.zolo.customer.callbacks;

/**
 * Created by surajit on 10/9/17.
 */

public interface RegisterView {

    void registrationValidation(boolean isRegister);

    void registrationFail(String message);

    void showPasswordStrength(String msg, String color);

    void hidePasswordStrength();

}
