package com.zolo.customer.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Surajit on 7/18/2016.
 */
public class SharedPreference extends AbsPrefs {


    private static final String PREF_KEY_USER_NAME = "Device_Name";
    private static final String PREF_DEFAULT_USER_NAME = "";

    public SharedPreference(Context context) {
        super(context);
    }


    public String getUserName() {
        return getString(PREF_KEY_USER_NAME,
                PREF_DEFAULT_USER_NAME);
    }

    public void setUserName(String userName) {
        setString(PREF_KEY_USER_NAME, userName);
    }


    public void clearPreference() {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
    }


}
