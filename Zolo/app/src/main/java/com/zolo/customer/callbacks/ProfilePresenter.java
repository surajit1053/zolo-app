package com.zolo.customer.callbacks;

/**
 * Created by surajit on 11/9/17.
 */

public interface ProfilePresenter {

    void upadateInfo(String email, String name);

    void callLogout();

}
