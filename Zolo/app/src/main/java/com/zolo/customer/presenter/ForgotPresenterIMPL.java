package com.zolo.customer.presenter;

import com.zolo.customer.AppController;
import com.zolo.customer.callbacks.ForgotPresenter;
import com.zolo.customer.callbacks.ForgotView;
import com.zolo.customer.db.DatabaseManager;
import com.zolo.customer.utils.SharedPreference;

import javax.inject.Inject;

/**
 * Created by surajit on 11/9/17.
 */

public class ForgotPresenterIMPL implements ForgotPresenter {

    @Inject
    DatabaseManager databaseManager;
    @Inject
    SharedPreference sharedPreference;

    private ForgotView forgotView;

    public ForgotPresenterIMPL(ForgotView forgotView) {
        this.forgotView = forgotView;
        ((AppController) AppController.getContext()).getDbComponent().inject(this);
    }

    @Override
    public void sendMail(String email) {
        if (email != null && email.length() > 0) {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                boolean is_email_exists = databaseManager.isEmailExists(email);
                if (is_email_exists) {
                    forgotView.OnSUccess();
                } else {
                    forgotView.onError("Email id doesn't exist");
                }
            } else {
                forgotView.onError("Please enter valid email id");
            }
        } else {
            forgotView.onError("Please enter email id");
        }
    }
}
