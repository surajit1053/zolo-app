package com.zolo.customer.presenter;

import com.zolo.customer.AppController;
import com.zolo.customer.callbacks.ProfilePresenter;
import com.zolo.customer.callbacks.ProfileView;
import com.zolo.customer.db.DatabaseManager;
import com.zolo.customer.db.User;
import com.zolo.customer.utils.SharedPreference;

import javax.inject.Inject;

/**
 * Created by surajit on 11/9/17.
 */

public class ProfilePresenterIMPL implements ProfilePresenter {

    private ProfileView profileView;
    @Inject
    DatabaseManager databaseManager;
    @Inject
    SharedPreference sharedPreference;

    public ProfilePresenterIMPL(ProfileView profileView) {
        this.profileView = profileView;
        ((AppController) AppController.getContext()).getDbComponent().inject(this);
        getUserInfo();
    }

    @Override
    public void upadateInfo(String email, String name) {
        if (email != null && email.length() > 0) {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                if (name != null && name.length() > 0) {
                    if (name.length() > 3) {
                        boolean is_update=databaseManager.updateUser(sharedPreference.getUserName(), email, name);
                        profileView.onUpdate(is_update);
                        if (is_update) {
                            getUserInfo();
                        }
                    } else {
                        profileView.onError("Name should have atlease 3 characters");
                    }
                } else {
                    profileView.onError("Please enter your name");
                }
            } else {
                profileView.onError("Please enter valid email id");
            }
        } else {
            profileView.onError("Please enter email id");
        }
    }

    @Override
    public void callLogout() {
        sharedPreference.clearPreference();
        profileView.onLogOut();
    }

    public void getUserInfo() {
        User user = databaseManager.getUser(sharedPreference.getUserName());
        if (user != null) {
            profileView.setData(user.getName(), user.getEmail(), user.getPhone());
        }
    }
}
