package com.zolo.customer.model;

import com.zolo.customer.presenter.ForgotPresenterIMPL;
import com.zolo.customer.presenter.LoginPresenterIMPL;
import com.zolo.customer.presenter.ProfilePresenterIMPL;
import com.zolo.customer.presenter.RegisterPresenterIMPL;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Module;

/**
 * Created by surajit on 10/9/17.
 */
@Singleton
@Component(modules = {DbModel.class})
public interface DbComponent {

    void inject(LoginPresenterIMPL loginPresenterIMPL);

    void inject(RegisterPresenterIMPL registerPresenterIMPL);

    void inject(ProfilePresenterIMPL profilePresenterIMPL);

    void inject(ForgotPresenterIMPL forgotPresenterIMPL);

}
