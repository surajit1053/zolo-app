package com.zolo.customer.presenter;

import com.zolo.customer.AppController;
import com.zolo.customer.callbacks.RegisterPresenter;
import com.zolo.customer.callbacks.RegisterView;
import com.zolo.customer.db.DatabaseManager;

import javax.inject.Inject;

/**
 * Created by surajit on 10/9/17.
 */

public class RegisterPresenterIMPL implements RegisterPresenter {

    private RegisterView registerView;
    @Inject
    DatabaseManager databaseManager;

    public RegisterPresenterIMPL(RegisterView registerView) {
        this.registerView = registerView;
        ((AppController) AppController.getContext()).getDbComponent().inject(this);
    }


    @Override
    public void registerData(String mobile, String email, String name, String password) {
        if (mobile != null && mobile.length() > 0) {
            if (mobile.length() > 9 && mobile.length() < 13) {
                if (email != null && email.length() > 0) {
                    if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                        if (name != null && name.length() > 0) {
                            if (name.length() > 3) {
                                if (password != null && password.length() > 0) {
                                    if (password.length() > 5) {
                                        registerView.registrationValidation(databaseManager.insertData(mobile, email, name, password));
                                    } else {
                                        registerView.registrationFail("Password lenth should be more than 5 characters");
                                    }
                                } else {
                                    registerView.registrationFail("Please enter password");
                                }
                            } else {
                                registerView.registrationFail("Name should have atlease 3 characters");
                            }
                        } else {
                            registerView.registrationFail("Please enter your name");
                        }
                    } else {
                        registerView.registrationFail("Please enter valid email id");
                    }
                } else {
                    registerView.registrationFail("Please enter email id");
                }
            } else {
                registerView.registrationFail("Please enter valid mobile number");
            }
        } else {
            registerView.registrationFail("Please enter mobile number");
        }
    }

    @Override
    public void passwordLength(String password) {
        setPasswordStrength(password.length());
    }


    private void setPasswordStrength(int length) {

        if (length == 0) {
            registerView.hidePasswordStrength();
            return;
        }

        if (length > 2 && length < 6) {
            registerView.showPasswordStrength("Password strength is Too Short", "#84FFFF");
        } else if (length > 5 && length < 8) {
            registerView.showPasswordStrength("Password strength is Strong", "#AED581");
        } else if (length > 7) {
            registerView.showPasswordStrength("Password strength is Very Strong", "#FFAB91");
        }
    }
}
