package com.zolo.customer.callbacks;

/**
 * Created by surajit on 10/9/17.
 */

public interface RegisterPresenter {
    void registerData(String mobile, String email, String name, String password);
    void passwordLength(String password);
}
