package com.zolo.customer.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class Roboto_Regular_Textview extends TextView {


    public Roboto_Regular_Textview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public Roboto_Regular_Textview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public Roboto_Regular_Textview(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        Typeface customFont = FontCache.getTypeface("Lato_Regular.ttf", context);
        setTypeface(customFont);
    }

}
