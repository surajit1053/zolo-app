package com.zolo.customer.callbacks;

/**
 * Created by surajit on 11/9/17.
 */

public interface ProfileView {

    void setData(String name, String email, String phone);

    void onUpdate(boolean isUpdate);

    void onLogOut();

    void onError(String message);

}
