package com.zolo.customer.db;

import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.zolo.customer.AppController;
import com.zolo.customer.R;

import java.sql.SQLException;

/**
 * Created by surajit on 9/9/17.
 */

public class DatabaseManager extends OrmLiteSqliteOpenHelper {

    private static final String DB_NAME = "zolo_database.db";
    private static final int DB_VERSION = 1;
    private Dao<User, Integer> userDao = null;

    public DatabaseManager() {
        super(AppController.context, DB_NAME, null, DB_VERSION, R.raw.db_schema);
        try {
            getChatsDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, User.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public Dao<User, Integer> getChatsDao() throws SQLException {
        if (userDao == null) {
            userDao = getDao(User.class);
        }
        return userDao;
    }


    public boolean insertData(String phone, String email, String name, String password) {

        if (isUserExists(phone)) {
            return false;
        }

        User user = new User();
        user.setPhone(phone);
        user.setEmail(email);
        user.setName(name);
        user.setPassword(password);
        try {
            userDao.createOrUpdate(user);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isUserExists(String phone) {
        try {
            User user = getChatsDao().queryBuilder().where().eq(User.COLUMN_PHONE, phone).queryForFirst();
            if (user != null) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public User getUser(String phone) {
        try {
            User user = getChatsDao().queryBuilder().where().eq(User.COLUMN_PHONE, phone).queryForFirst();
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public boolean updateUser(String phone, String email, String name) {
        try {
            User user = getChatsDao().queryBuilder().where().eq(User.COLUMN_PHONE, phone).queryForFirst();
            user.setName(name);
            user.setEmail(email);
            userDao.createOrUpdate(user);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public boolean login(String phone, String password) {
        try {
            User user = getChatsDao().queryBuilder().where().eq(User.COLUMN_PHONE, phone).and().eq(User.COLUMN_PASSWORD, password).queryForFirst();
            if (user != null) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isEmailExists(String email) {
        try {
            User user = getChatsDao().queryBuilder().where().eq(User.COLUMN_EMAIL, email).queryForFirst();
            if (user != null) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


}
