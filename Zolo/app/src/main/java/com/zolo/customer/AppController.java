package com.zolo.customer;

import android.app.Application;
import android.content.Context;

import com.zolo.customer.model.DaggerDbComponent;
import com.zolo.customer.model.DbComponent;
import com.zolo.customer.model.DbModel;

/**
 * Created by surajit on 10/9/17.
 */

public class AppController extends Application {


    public static Context context;
    private DbComponent dbComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        dbComponent = DaggerDbComponent.builder()
                .dbModel(new DbModel(this))
                .build();
    }

    public static Context getContext() {
        return context;
    }

    public DbComponent getDbComponent() {
        return dbComponent;
    }
}
