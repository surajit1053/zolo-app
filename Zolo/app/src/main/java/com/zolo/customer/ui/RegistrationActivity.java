package com.zolo.customer.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.zolo.customer.R;
import com.zolo.customer.callbacks.RegisterView;
import com.zolo.customer.presenter.RegisterPresenterIMPL;
import com.zolo.customer.utils.CommonMethods;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by surajit on 10/9/17.
 */

public class RegistrationActivity extends BaseActivity implements RegisterView, TextWatcher, View.OnFocusChangeListener {

    @Nullable
    @BindView(R.id.et_reg_mobile)
    EditText et_mobile;

    @Nullable
    @BindView(R.id.et_reg_mail)
    EditText et_mail;

    @Nullable
    @BindView(R.id.et_reg_name)
    EditText et_name;

    @Nullable
    @BindView(R.id.et_reg_password)
    EditText et_password;

    @Nullable
    @BindView(R.id.reg_img_show_password)
    ImageView imageView;

    @Nullable
    @BindView(R.id.tv_password_strength)
    TextView tv_password_strength;

    private RegisterPresenterIMPL registerPresenterIMPL;

    private boolean isChecked = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        registerPresenterIMPL = new RegisterPresenterIMPL(this);
        et_password.addTextChangedListener(this);
        et_password.setOnFocusChangeListener(this);
    }

    @Optional
    @OnClick(R.id.reg_log_in)
    public void sendToPreviousActivity() {
        finish();
    }

    @Optional
    @OnClick(R.id.button_register)
    public void register() {
        registerPresenterIMPL.registerData(et_mobile.getText().toString().trim(), et_mail.getText().toString().trim(), et_name.getText().toString().trim(), et_password.getText().toString().trim());
    }

    @Optional
    @OnClick(R.id.reg_img_show_password)
    public void show_hide_password() {
        isChecked = CommonMethods.getInstance().setPasswordIconStatus(et_password, imageView, isChecked);
    }


    @Override
    public void registrationValidation(boolean isRegister) {
        if (isRegister) {
            CommonMethods.getInstance().showToast(this, "Registration has been done successfully");
            finish();
        } else {
            showSnackBarMessage("This phone number already exists");
        }
    }

    @Override
    public void registrationFail(String message) {
        if (tv_password_strength.getVisibility() == View.VISIBLE) {
            tv_password_strength.setVisibility(View.GONE);
        }
        showSnackBarMessage(message);
    }

    @Override
    public void showPasswordStrength(String msg, String color) {
        if (tv_password_strength.getVisibility() == View.GONE) {
            tv_password_strength.setVisibility(View.VISIBLE);
        }
        tv_password_strength.setText(msg);
        tv_password_strength.setTextColor(Color.parseColor(color));
    }

    @Override
    public void hidePasswordStrength() {
        if (tv_password_strength.getVisibility() == View.VISIBLE) {
            tv_password_strength.setVisibility(View.GONE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        registerPresenterIMPL.passwordLength(editable.toString().trim());
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            tv_password_strength.setVisibility(View.GONE);
        }
    }
}
