package com.zolo.customer.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class Roboto_Light_Edittext extends EditText {

    public Roboto_Light_Edittext(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public Roboto_Light_Edittext(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public Roboto_Light_Edittext(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        Typeface customFont = FontCache.getTypeface("Lato_Light.ttf", context);
        setTypeface(customFont);
    }
}
