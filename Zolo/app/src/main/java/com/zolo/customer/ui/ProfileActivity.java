package com.zolo.customer.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;

import com.zolo.customer.R;
import com.zolo.customer.callbacks.ProfileView;
import com.zolo.customer.presenter.ProfilePresenterIMPL;
import com.zolo.customer.utils.CommonMethods;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by surajit on 11/9/17.
 */

public class ProfileActivity extends BaseActivity implements ProfileView {

    @Nullable
    @BindView(R.id.et_profile_name)
    EditText et_name;

    @Nullable
    @BindView(R.id.et_profile_email)
    EditText et_email;

    @Nullable
    @BindView(R.id.et_profile_mobile)
    EditText et_phone;

    private ProfilePresenterIMPL profilePresenterIMPL;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        et_phone.setEnabled(false);
        profilePresenterIMPL = new ProfilePresenterIMPL(this);
    }

    @Override
    public void setData(String name, String email, String phone) {
        et_phone.setText(phone);
        et_email.setText(email);
        et_name.setText(name);
    }

    @Optional
    @OnClick(R.id.log_out)
    public void logout() {
        profilePresenterIMPL.callLogout();
    }

    @Optional
    @OnClick(R.id.button_update)
    public void updateUserInfo() {
        profilePresenterIMPL.upadateInfo(et_email.getText().toString().trim(), et_name.getText().toString().trim());
    }


    @Override
    public void onUpdate(boolean isUpdate) {
        if (isUpdate) {
            showSnackBarMessage("Updated successfully");
        } else {
            showSnackBarMessage("Some error occurred");
        }
    }

    @Override
    public void onLogOut() {
        CommonMethods.getInstance().showToast(this, "Logout successfully");
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onError(String message) {
        showSnackBarMessage(message);
    }
}
