package com.zolo.customer.callbacks;

/**
 * Created by surajit on 11/9/17.
 */

public interface ForgotPresenter {
    void sendMail(String mail);
}
