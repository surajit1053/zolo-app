package com.zolo.customer.presenter;

import com.zolo.customer.AppController;
import com.zolo.customer.callbacks.LoginPresenter;
import com.zolo.customer.callbacks.LoginView;
import com.zolo.customer.db.DatabaseManager;
import com.zolo.customer.utils.SharedPreference;

import javax.inject.Inject;

/**
 * Created by surajit on 8/9/17.
 */

public class LoginPresenterIMPL implements LoginPresenter {

    private LoginView loginView;
    @Inject
    DatabaseManager databaseManager;
    @Inject
    SharedPreference sharedPreference;

    public LoginPresenterIMPL(LoginView loginView) {
        this.loginView = loginView;
        ((AppController) AppController.getContext()).getDbComponent().inject(this);
    }

    @Override
    public void loginData(String mobile, String password) {
        if (mobile != null && mobile.trim().length() > 0) {
            if (mobile.length() > 9 && mobile.length() < 13) {
                if (password != null && password.trim().length() > 0) {
                    boolean isLogin = databaseManager.login(mobile, password);
                    if (isLogin) {
                        sharedPreference.setUserName(mobile);
                    }
                    loginView.loginValidation(isLogin);
                } else {
                    loginView.loginFail("Please enter password");
                }
            } else {
                loginView.loginFail("Please enter valid mobile number");
            }
        } else {
            loginView.loginFail("Please enter mobile number");
        }
    }
}
