package com.zolo.customer.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class Roboto_Light_Textview extends TextView {

	public Roboto_Light_Textview(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public Roboto_Light_Textview(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public Roboto_Light_Textview(Context context) {
		super(context);
		init(context);
	}

	private void init(Context context) {
		Typeface customFont = FontCache.getTypeface("Lato_Light.ttf", context);
		setTypeface(customFont);
	}

}
