package com.zolo.customer.callbacks;

/**
 * Created by surajit on 9/9/17.
 */

public interface LoginPresenter {
    void loginData(String mobile, String password);
}
