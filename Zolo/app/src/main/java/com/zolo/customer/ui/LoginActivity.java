package com.zolo.customer.ui;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.widget.EditText;
import android.widget.ImageView;

import com.zolo.customer.R;
import com.zolo.customer.callbacks.LoginView;
import com.zolo.customer.presenter.LoginPresenterIMPL;
import com.zolo.customer.utils.CommonMethods;
import com.zolo.customer.utils.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;

public class LoginActivity extends BaseActivity implements LoginView {

    private LoginPresenterIMPL loginPresenterIMPL;
    @Nullable
    @BindView(R.id.et_mobile)
    EditText et_mobile;
    @Nullable
    @BindView(R.id.et_password)
    EditText et_password;

    @Nullable
    @BindView(R.id.login_img_show_password)
    ImageView imageView;

    private boolean isChecked = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkAutoLogin();
        loginPresenterIMPL = new LoginPresenterIMPL(this);
        initContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    private void checkAutoLogin() {
        if (new SharedPreference(this).getUserName().length() > 0) {
            sendToProfilePage();
        }
    }

    @Optional
    @OnClick(R.id.button_login)
    public void submit() {
        loginPresenterIMPL.loginData(et_mobile.getText().toString().trim(), et_password.getText().toString().trim());
    }

    @Optional
    @OnClick(R.id.create_account)
    public void visit_registration_page() {
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }

    @Optional
    @OnClick(R.id.forgot_password)
    public void visit_forgot_password() {
        Intent intent = new Intent(this, ForgorPasswordActivity.class);
        startActivity(intent);
    }

    @Optional
    @OnClick(R.id.login_img_show_password)
    public void show_hide_password() {
        isChecked = CommonMethods.getInstance().setPasswordIconStatus(et_password, imageView, isChecked);
    }


    @Override
    public void loginValidation(boolean isLogin) {
        if (isLogin) {
            CommonMethods.getInstance().showToast(this, "Login successfully");
            sendToProfilePage();
        } else {
            showSnackBarMessage("Invalid Mobile or Password");
        }
    }

    private void sendToProfilePage() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void loginFail(String message) {
        showSnackBarMessage(message);
    }


}
