package com.zolo.customer.callbacks;

/**
 * Created by surajit on 9/9/17.
 */

public interface LoginView {

    void loginValidation(boolean isLogin);

    void loginFail(String message);

}
