package com.zolo.customer.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.TextView;

import com.zolo.customer.R;
import com.zolo.customer.callbacks.ForgotView;
import com.zolo.customer.presenter.ForgotPresenterIMPL;
import com.zolo.customer.utils.CommonMethods;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by surajit on 10/9/17.
 */

public class ForgorPasswordActivity extends BaseActivity implements ForgotView {

    @Nullable
    @BindView(R.id.tv_text)
    TextView textView;

    @Nullable
    @BindView(R.id.et_rseset_email)
    EditText et_email;

    private ForgotPresenterIMPL forgotPresenterIMPL;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initContentView(R.layout.activity_forgotpassword);
        ButterKnife.bind(this);
        textView.setText("FORGOT \n PASSWORD?");
        forgotPresenterIMPL = new ForgotPresenterIMPL(this);
    }


    @Optional
    @OnClick(R.id.reg_log_in)
    public void sendToPreviousActivity() {
        finish();
    }

    @Optional
    @OnClick(R.id.button_reset_password)
    public void resetPassword() {
        forgotPresenterIMPL.sendMail(et_email.getText().toString().trim());
    }


    @Override
    public void OnSUccess() {
        CommonMethods.getInstance().showToast(this, "New Password has been sent to your registered mail id.");
        finish();
    }

    @Override
    public void onError(String message) {
        showSnackBarMessage(message);
    }
}
