package com.zolo.customer.model;

import android.content.Context;

import com.zolo.customer.db.DatabaseManager;
import com.zolo.customer.utils.SharedPreference;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by surajit on 10/9/17.
 */

@Module
public class DbModel {

    private Context context;

    public DbModel(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    public DatabaseManager getDbInstance() {
        DatabaseManager databaseManager = new DatabaseManager();
        return databaseManager;
    }

    @Singleton
    @Provides
    public SharedPreference getSharedPreference() {
        return new SharedPreference(context);
    }

}
